import { transposeMatrix } from './transposeMatrix';

describe('transposeMatrix', () => {
  it('transposeMatrix rotates matrix to the right', () => {
    expect(transposeMatrix([[1,2,3],[23,24,25]])).toEqual([[23,1],[24,2],[25,3]]);
  });

  it('transposeMatrix rotates single item', () => {
    expect(transposeMatrix([[10]])).toEqual([[10]]);
  });

  it('transposeMatrix returns the same matrix', () => {
      expect(transposeMatrix([[]])).toEqual([[]]);
  });
});

import { swapKeysAndValues } from './swapKeysAndValues';

describe('swapKeysAndValues', () => {
  it('swapKeysAndValues swaps integers', () => {
    expect(swapKeysAndValues({
        "i": "am",
        "he": "is"
    }))
    .toEqual({
        "am": "i",
        "is": "he"
    });
  });

  it('swapKeysAndValues swaps strings', () => {
        expect(swapKeysAndValues({
        'this': 'is',
        'a': 'test',
    }))
    .toEqual({
        'is': 'this',
        'test': 'a',
    });
  });
});
